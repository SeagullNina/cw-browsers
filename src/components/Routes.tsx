import React from 'react'
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom'
import MainPage from './MainPage'
import GoogleChrome from './GoogleChrome'
import Yandex from "./Yandex";
import Mozilla from "./Mozilla";
import Feed from "./Feed";

const Routes = React.memo(() => {
    let log = !!localStorage.getItem('logged');
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/auth/">
                    <MainPage login={true} />
                </Route>
                <Route path="/registration/">
                    <MainPage login={false} />
                </Route>
                <Route path="/google/">
                    <GoogleChrome/>
                </Route>
                <Route path="/yandex/">
                    <Yandex/>
                </Route>
                <Route path="/mozilla/">
                    <Mozilla/>
                </Route>
                <Route path="/">
                    <Feed logged={log}/>
                </Route>
            </Switch>
        </BrowserRouter>
    )
})

export default Routes
