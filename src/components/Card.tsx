import React from "react";
import styles from './Card.module.scss'
interface IProps {
    title: string,
    content: string
}

const Card = (props: IProps) =>
{
    return <div className={styles.card}>
        <h1>{props.title}</h1>
        <div className={styles.info}>{props.content}</div>
    </div>
}
export default Card