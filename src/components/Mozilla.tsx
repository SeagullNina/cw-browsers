import React from "react";
import styles from './Mozilla.module.scss'
import Navigation from "./Navigation";

const Mozilla = () => {
    return (
      <div className={styles.page}>
        <Navigation />
        <div className={styles.content}>
          <img alt="" src="../Mozilla.jpg" className={styles.img} />
          <div className={styles.info}>
            Mozilla Firefox — свободный браузер на движке Gecko, разработкой и
            распространением которого занимается Mozilla Corporation.
            <br />
            Второй по популярности браузер в мире и первый среди свободного ПО —
            в июне 2016 года его рыночная доля составила 14,15 %. Браузер имеет
            особенный успех в некоторых странах, в частности, в Германии это
            самый популярный браузер с долей 34,27 % на июнь 2016 года.
            <br />В России Firefox занимает третье место по популярности среди
            браузеров для ПК с долей 8,74 % пользователей на декабрь 2018 года,
            немного опережая Opera.
            <br />В браузере присутствует интерфейс со многими вкладками,
            проверка орфографии, поиск по мере набора, «живые закладки»,
            менеджер загрузок, поле для обращения к поисковым системам. Новые
            функции можно добавлять при помощи расширений.
          </div>
        </div>
      </div>
    );
}

export default Mozilla