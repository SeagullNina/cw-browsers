import React from "react";
import styles from "./AuthForm.module.scss";
import UserApi from "../../classes/api/UserApi";

const AuthForm = () => {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [error, setError] = React.useState(false);
  const [email, setEmail] = React.useState<string>("");
  const [password, setPassword] = React.useState<string>("");
  return (
    <div className={styles.page}>
      <div className={styles.header}>Авторизация</div>
      <div className={styles.form}>
        <span>Email</span>
        <input
          id="email"
          className={styles.input}
          type="text"
          onChange={event => {
            error && setError(false);
            setEmail(event.target.value);
          }}
          placeholder={"Введите ваш электронный адрес"}
        />
        <span>Пароль</span>
        <input
          id="password"
          className={styles.input}
          type="password"
          onChange={event => {
            error && setError(false);
            setPassword(event.target.value);
          }}
          placeholder={"Введите ваш пароль"}
        />
        <span className={error ? styles.error : styles.none}>
          Неправильно введен логин или пароль.
        </span>
      </div>
      <input
        disabled={loading}
        className={styles.button}
        type="button"
        value="Авторизоваться"
        onClick={() => {
          setLoading(true);
          UserApi.auth(email, password)
            .then(() => {
              setLoading(false);
              window.location.href = "/";
              localStorage.setItem("logged", "1");
            })
            .catch(() => {
              setLoading(false);
              setError(true);
            });
        }}
      />
    </div>
  );
};
export default AuthForm;
