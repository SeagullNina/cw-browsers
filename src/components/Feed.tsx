import React, { useContext, useEffect, useState } from "react";
import styles from "./Feed.module.scss";
import Navigation from "./Navigation";
import Card from "./Card";
import ArticleApi from "../classes/api/ArticleApi";
import { IArticle } from "../classes/models/IArticle";

interface IProps {
  logged: boolean;
}

const Feed = (props: IProps) => {
  const [news, setNews] = React.useState<IArticle[]>([]);
  const [title, setTitle] = React.useState("");
  const [content, setContent] = React.useState("");

  useEffect(() => {
    ArticleApi.getArticles().then(result => setNews(result));
  }, []);

  return (
    <div className={styles.page}>
      <Navigation />
      <div className={styles.content}>
        <div className={props.logged ? styles.add : styles.none}>
          <input
            type="text"
            className={styles.title}
            onChange={event => {
              setTitle(event.target.value);
            }}
          />
          <textarea
            cols={39}
            rows={12}
            onChange={event => {
              setContent(event.target.value);
            }}
          ></textarea>
          <button
            className={styles.button}
            onClick={() => {
              const newArticle: IArticle = { title, content };
              setNews(prevState => [...prevState, newArticle]);
              setTitle("");
              setContent("");
              ArticleApi.publishArticle(title, content);
            }}
          >
            Отправить
          </button>
        </div>
      </div>
      <div className={styles.cards}>
        {news.map((article: IArticle) => {
          return <Card title={article.title} content={article.content} />;
        })}
      </div>
    </div>
  );
};

export default Feed;
