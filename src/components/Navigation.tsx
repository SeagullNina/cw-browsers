import React from "react";
import styles from "./Navigation.module.scss";
import { NavLink } from "react-router-dom";
import classNames from "classnames";

const Navigation = () => {
  return (
    <div className={styles.mainPage}>
      <div className={styles.navigation}>
        <div className={styles.logoBlock}>
          <img alt="" className={styles.logo} src="../mainLogo.png" />
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            exact
            to="/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Новости браузеров</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/google/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Google Chrome</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/yandex/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Яндекс.Браузер</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/mozilla/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Mozilla Firefox</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/auth/"
            className={classNames(
              styles.link,
              window.location.pathname === "/registration/"
                ? styles.selected
                : ""
            )}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Вход / Регистрация</span>
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Navigation;
