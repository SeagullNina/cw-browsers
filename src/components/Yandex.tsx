import React from "react";
import styles from './Yandex.module.scss'
import Navigation from "./Navigation";

const Yandex = () => {
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.content}>
            <img alt="" src="../Yandex.png" className={styles.img}/>
            <div className={styles.info}>
                Яндекс.Браузер — браузер, созданный компанией «Яндекс» на основе движка Blink, используемого в открытом браузере Chromium. Впервые был представлен 1 октября 2012 года на технологической конференции Yet another Conference.
                <br/>
                Обозреватель от Яндекса занимает второе место на рынке настольных компьютеров в рунете. По состоянию на декабрь 2019 года, доля обозревателя среди всех устройств составляет в Рунете 20,05 %
                <br/>
                Яндекс.Браузер одновременно поддерживает расширения от Chromium и Opera, которые можно установить из интернет-магазина Chrome и Opera Addons.
                <br/>
                У Яндекс.Браузера адресная строка называется «Умной строкой» из-за дополнительных полезных возможностей, отсутствующих в Chromium'е.
            </div>
        </div>
    </div>
}

export default Yandex