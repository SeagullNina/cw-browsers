import { IUser } from '../models/IUser'
import axios, { AxiosInstance } from 'axios'

class UserApi {
    private http: AxiosInstance = axios.create({
        baseURL: 'https://cv-browser-backend.herokuapp.com/api',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json;charset=utf-8',
        },
    })

    public registration = async (user: IUser) => {
        return await this.http.post('/registration', { ...user })
    }

    public auth = async (email: string, password: string) => {
        return await this.http.post('/auth', { email, password })
    }
}

export default new UserApi()