import axios, { AxiosInstance } from 'axios'
import { IArticle } from '../models/IArticle'

class ArticleApi {
    private http: AxiosInstance = axios.create({
        baseURL: 'https://cv-browser-backend.herokuapp.com/api/article',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json;charset=utf-8',
        },
    })

    public getArticles = async (): Promise<IArticle[]> =>{
        const fetchedArticles = await this.http.get("")
        return fetchedArticles.data;
    }
    public publishArticle = async (title:string, content:string) =>{
        return await this.http.post("", {title, content})
    }
}

export default new ArticleApi()