const mongoose = require('mongoose');

const { Schema } = mongoose;

const definition = {
    email: {
        type: String,
    },
    password: {
        type: String,
    }
};

const options = {
    timestamps: true
};

const UserSchema = new Schema(definition, options);

module.exports = mongoose.model('User', UserSchema);