const repository = require('./repository');

exports.login = async (req, res) => {
    const { email, password } = req.body;
    const authedUser = await repository.findUser(email, password);
    if (authedUser && authedUser._id) {
        res.send(authedUser);
    } else {
        res.status(400);
    }
};

exports.register = (req, res) => {
    const data = req.body
    repository.saveUser({
        ...data,
        achievements: [],
        dateOfReg: new Date()
    }, (err, user) => {
        if (err) {
            return res.validationError(err);
        }
        return res.send(user);
    });
};
